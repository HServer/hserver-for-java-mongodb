package com.test.dao;

import com.test.bean.Employee;
import dev.morphia.Datastore;
import dev.morphia.query.Query;
import dev.morphia.query.experimental.filters.Filters;
import cn.hserver.core.ioc.annotation.Autowired;
import cn.hserver.core.ioc.annotation.Bean;

import java.util.List;

@Bean
public class EmployeeDao {

    @Autowired
    private Datastore datastore;

    public void save() {
        final Employee elmer = new Employee("Elmer Fudd", 50000.0);
        datastore.save(elmer);
        final Employee daffy = new Employee("Daffy Duck", 40000.0);
        datastore.save(daffy);
        final Employee pepe = new Employee("Pepé Le Pew", 25000.0);
        datastore.save(pepe);
        elmer.getDirectReports().add(daffy);
        elmer.getDirectReports().add(pepe);
        datastore.save(elmer);
    }

    public List<Employee> queryList() {
        Query<Employee> query = datastore.find(Employee.class);
        List<Employee> employees = query.iterator().toList();
        return employees;
    }


    public List<Employee> queryList1() {
        return datastore.find(Employee.class)
                .filter(Filters.lte("salary", 30000))
                .iterator()
                .toList();
    }



}
