package com.test.config;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import dev.morphia.Datastore;
import dev.morphia.Morphia;
import cn.hserver.core.ioc.annotation.Bean;
import cn.hserver.core.ioc.annotation.Configuration;
import cn.hserver.core.ioc.annotation.Value;

@Configuration
public class MongoConfig {

    @Value("mongo.url")
    private String connect;

    @Bean
    public Datastore datastore() {
        MongoClient mongoClient = MongoClients.create(connect);
        Datastore datastore = Morphia.createDatastore(mongoClient, "morphia_example");
        datastore.getMapper().mapPackage("com.test.bean");
        datastore.ensureIndexes();
        return datastore;
    }

}
