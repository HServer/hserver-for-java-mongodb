package com.test.controller;

import cn.hserver.plugin.web.annotation.Controller;
import cn.hserver.plugin.web.annotation.GET;
import com.test.dao.EmployeeDao;
import cn.hserver.core.ioc.annotation.Autowired;
import cn.hserver.core.server.util.JsonResult;

@Controller
public class TestController {

    @Autowired
    private EmployeeDao employeeDao;

    @GET("/save")
    public JsonResult save() {
        employeeDao.save();
        return JsonResult.ok();
    }

    @GET("/query1")
    public JsonResult query1() {
        return JsonResult.ok().put("data", employeeDao.queryList());
    }
    @GET("/query2")
    public JsonResult query2() {
        return JsonResult.ok().put("data", employeeDao.queryList1());
    }

}
